1
00:00:00,000 --> 00:00:03,840
okay so at this point you should have

2
00:00:01,439 --> 00:00:04,319
checked out the architecture 1001 source

3
00:00:03,840 --> 00:00:07,520
code

4
00:00:04,319 --> 00:00:07,839
go ahead and go into there and within

5
00:00:07,520 --> 00:00:10,080
there

6
00:00:07,839 --> 00:00:11,519
the first example is actually in

7
00:00:10,080 --> 00:00:14,320
ExampleSubroutine1

8
00:00:11,519 --> 00:00:15,759
and CallAFunction1.c was the first

9
00:00:14,320 --> 00:00:16,080
thing that we had looked at in visual

10
00:00:15,759 --> 00:00:17,039
studio

11
00:00:16,080 --> 00:00:19,119
and that's the first thing we're going

12
00:00:17,039 --> 00:00:19,680
to look at here now in order to compile

13
00:00:19,119 --> 00:00:23,519
it

14
00:00:19,680 --> 00:00:25,439
we're going to use the no

15
00:00:23,519 --> 00:00:27,439
stack protector option we're going to

16
00:00:25,439 --> 00:00:28,960
use this option on all of the things

17
00:00:27,439 --> 00:00:30,480
even though they don't necessarily all

18
00:00:28,960 --> 00:00:32,239
need it but this will just make the

19
00:00:30,480 --> 00:00:34,399
assembly simpler in the same way that we

20
00:00:32,239 --> 00:00:36,640
reconfigured the visual studio stuff so

21
00:00:34,399 --> 00:00:38,079
you could give it an output and call it

22
00:00:36,640 --> 00:00:40,719
you know something

23
00:00:38,079 --> 00:00:42,320
but basically in order to make it easier

24
00:00:40,719 --> 00:00:43,680
to use the same commands between

25
00:00:42,320 --> 00:00:45,200
multiple examples that we're going to be

26
00:00:43,680 --> 00:00:47,600
doing in rapid succession

27
00:00:45,200 --> 00:00:49,120
i'm just going to give no output command

28
00:00:47,600 --> 00:00:51,920
and then the result of that

29
00:00:49,120 --> 00:00:55,120
is that it's going to name it a.out

30
00:00:51,920 --> 00:00:58,239
now we can debug a.out with gdb

31
00:00:55,120 --> 00:01:00,800
and you should have a configuration file

32
00:00:58,239 --> 00:01:01,600
in your home directory so mine's named

33
00:01:00,800 --> 00:01:05,119
gdb

34
00:01:01,600 --> 00:01:05,920
config and yours may differ from this

35
00:01:05,119 --> 00:01:08,560
slightly

36
00:01:05,920 --> 00:01:10,000
but basically i'm displaying you know 10

37
00:01:08,560 --> 00:01:12,240
instructions at rip

38
00:01:10,000 --> 00:01:13,520
most of the common registers except some

39
00:01:12,240 --> 00:01:16,000
of the higher ones

40
00:01:13,520 --> 00:01:17,119
and some stuff for the stack and then

41
00:01:16,000 --> 00:01:21,119
starting the program

42
00:01:17,119 --> 00:01:24,159
so we would do gdb a.out

43
00:01:21,119 --> 00:01:27,840
-q for quiet and -x

44
00:01:24,159 --> 00:01:27,840
for our gdp config

45
00:01:29,040 --> 00:01:32,560
if we do that then it goes ahead and

46
00:01:30,880 --> 00:01:34,320
starts it up quiet mode

47
00:01:32,560 --> 00:01:36,159
and immediately starts it because of the

48
00:01:34,320 --> 00:01:36,640
start command in there and what we'll

49
00:01:36,159 --> 00:01:39,840
see

50
00:01:36,640 --> 00:01:41,520
is basically 10 assembly instructions

51
00:01:39,840 --> 00:01:42,799
all of our registers that we care to

52
00:01:41,520 --> 00:01:46,320
look at at the moment

53
00:01:42,799 --> 00:01:48,079
and then a updating view of the stack

54
00:01:46,320 --> 00:01:49,960
now the first assembly instruction that

55
00:01:48,079 --> 00:01:53,360
we see here is called

56
00:01:49,960 --> 00:01:54,079
endbr64 and so i'll just you know save you

57
00:01:53,360 --> 00:01:57,520
the mystery

58
00:01:54,079 --> 00:01:58,719
there this endbr64 is for a new

59
00:01:57,520 --> 00:02:00,640
technology

60
00:01:58,719 --> 00:02:02,000
that's called intel control flow

61
00:02:00,640 --> 00:02:05,119
enhancement technology

62
00:02:02,000 --> 00:02:06,719
and so while there's a whole 358 page

63
00:02:05,119 --> 00:02:08,720
manual you can read to learn about

64
00:02:06,719 --> 00:02:10,319
control flow enhancement technology

65
00:02:08,720 --> 00:02:12,879
the important thing that we care about

66
00:02:10,319 --> 00:02:13,599
for now is that it says the endbr32

67
00:02:12,879 --> 00:02:15,680
and endbr64

68
00:02:13,599 --> 00:02:17,760
instructions will have the same

69
00:02:15,680 --> 00:02:19,840
effect as a nop instruction on Intel

70
00:02:17,760 --> 00:02:21,520
64 processors that do not support

71
00:02:19,840 --> 00:02:22,000
control flow enhancement technology so

72
00:02:21,520 --> 00:02:24,319
basically

73
00:02:22,000 --> 00:02:25,920
it's saying the compiler is forcing this

74
00:02:24,319 --> 00:02:28,000
assembly instruction in here

75
00:02:25,920 --> 00:02:29,840
so that if this code ends up being run

76
00:02:28,000 --> 00:02:31,280
on some new system that supports this

77
00:02:29,840 --> 00:02:33,680
new security mechanism

78
00:02:31,280 --> 00:02:35,120
then it'll add some extra security and

79
00:02:33,680 --> 00:02:36,480
if it doesn't then it's just going to

80
00:02:35,120 --> 00:02:36,959
act like a nop and it's going to do

81
00:02:36,480 --> 00:02:38,560
nothing

82
00:02:36,959 --> 00:02:40,560
so for all intents and purposes

83
00:02:38,560 --> 00:02:42,319
basically just assume endbr

84
00:02:40,560 --> 00:02:44,239
is going to be a nop until you later

85
00:02:42,319 --> 00:02:46,160
on you know can learn more about that

86
00:02:44,239 --> 00:02:48,319
technology i'll put a link to

87
00:02:46,160 --> 00:02:50,879
a article that talks about how windows

88
00:02:48,319 --> 00:02:52,879
uses that technology for instance

89
00:02:50,879 --> 00:02:55,200
next to this video so there's not much

90
00:02:52,879 --> 00:02:57,040
new and interesting about this assembly

91
00:02:55,200 --> 00:02:58,480
basically the typical things we would

92
00:02:57,040 --> 00:03:00,800
expect to see the

93
00:02:58,480 --> 00:03:02,720
AT&T syntax means there's percent signs

94
00:03:00,800 --> 00:03:03,599
in front of registers and dollar signs

95
00:03:02,720 --> 00:03:06,480
in front of

96
00:03:03,599 --> 00:03:06,800
immediates so it's going to do this first

97
00:03:06,480 --> 00:03:08,879
nop

98
00:03:06,800 --> 00:03:10,080
so i'm going to do ni for next

99
00:03:08,879 --> 00:03:13,519
assembly instruction

100
00:03:10,080 --> 00:03:15,760
step over that you know then push rbp

101
00:03:13,519 --> 00:03:17,519
so do that and there we go we see 0

102
00:03:15,760 --> 00:03:20,720
went on to the top of the stack

103
00:03:17,519 --> 00:03:21,120
and rbp is currently set to 0 that's

104
00:03:20,720 --> 00:03:24,560
why

105
00:03:21,120 --> 00:03:26,239
then move rsp to rbp so AT&T syntax

106
00:03:24,560 --> 00:03:28,959
moving from left to right

107
00:03:26,239 --> 00:03:30,239
so move rsp which is 0x7ffff some

108
00:03:28,959 --> 00:03:33,440
stack location to

109
00:03:30,239 --> 00:03:37,200
rbp so step over that and there you go

110
00:03:33,440 --> 00:03:39,280
same thing mov 0 to eax

111
00:03:37,200 --> 00:03:40,799
step over that and now we're going to

112
00:03:39,280 --> 00:03:42,480
want to step into

113
00:03:40,799 --> 00:03:44,400
this function that's going to be called

114
00:03:42,480 --> 00:03:47,360
function func so

115
00:03:44,400 --> 00:03:47,920
si to step into once again we have you

116
00:03:47,360 --> 00:03:49,599
know this

117
00:03:47,920 --> 00:03:51,280
nop instruction this control flow

118
00:03:49,599 --> 00:03:53,040
enforcement technology which we're going

119
00:03:51,280 --> 00:03:56,480
to just assume is not doing anything

120
00:03:53,040 --> 00:03:57,360
so nop step over again it's creating a

121
00:03:56,480 --> 00:03:59,840
stack frame

122
00:03:57,360 --> 00:04:02,400
because basically optimizations are

123
00:03:59,840 --> 00:04:04,400
disabled by default in gcc if you don't

124
00:04:02,400 --> 00:04:05,680
you know specify any optimizations so

125
00:04:04,400 --> 00:04:09,200
create a stack frame

126
00:04:05,680 --> 00:04:11,200
push rbp then move rsp to rbp

127
00:04:09,200 --> 00:04:12,319
now you can see that the value that got

128
00:04:11,200 --> 00:04:16,440
pushed was the

129
00:04:12,319 --> 00:04:19,359
old value of rbp 0xfff

130
00:04:16,440 --> 00:04:23,280
dcf60 and then it's going to just

131
00:04:19,359 --> 00:04:25,919
mov 0xbeef into eax and this is just

132
00:04:23,280 --> 00:04:27,680
always unconditionally done and then

133
00:04:25,919 --> 00:04:30,160
it's going to exit out of the function

134
00:04:27,680 --> 00:04:31,919
and tear down the stack frame so pop rbp

135
00:04:30,160 --> 00:04:34,000
this old value of rbp

136
00:04:31,919 --> 00:04:35,280
into the register there we go now it's

137
00:04:34,000 --> 00:04:37,360
got the old value and

138
00:04:35,280 --> 00:04:40,320
return so it's going to take that value

139
00:04:37,360 --> 00:04:42,080
off the stack put it into rip

140
00:04:40,320 --> 00:04:44,639
and there we go we're back where we came

141
00:04:42,080 --> 00:04:46,479
from but now it's going to just always

142
00:04:44,639 --> 00:04:47,840
put 0xf00d into eax

143
00:04:46,479 --> 00:04:49,919
it doesn't care about the fact that we

144
00:04:47,840 --> 00:04:51,280
returned 0xbeef and rax

145
00:04:49,919 --> 00:04:52,960
it's basically just always

146
00:04:51,280 --> 00:04:55,440
unconditionally returning 0xf00d

147
00:04:52,960 --> 00:04:57,280
so continue and then now we're into the

148
00:04:55,440 --> 00:05:00,479
function epilogue and we're tearing down

149
00:04:57,280 --> 00:05:04,000
the stack frame for main

150
00:05:00,479 --> 00:05:05,919
next and return and now we're out in the

151
00:05:04,000 --> 00:05:07,280
function that called main and we don't

152
00:05:05,919 --> 00:05:09,280
particularly care about that

153
00:05:07,280 --> 00:05:11,039
so we're done with this example nothing

154
00:05:09,280 --> 00:05:14,160
new and interesting there so let's move

155
00:05:11,039 --> 00:05:14,160
on to the next example

