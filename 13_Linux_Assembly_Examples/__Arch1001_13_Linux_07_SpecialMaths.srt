1
00:00:00,080 --> 00:00:03,360
okay the next example is SpecialMaths

2
00:00:02,800 --> 00:00:06,879
which

3
00:00:03,360 --> 00:00:10,080
before we went into specifically to

4
00:00:06,879 --> 00:00:11,519
see whether or not the special math form

5
00:00:10,080 --> 00:00:13,599
that we chose would cause an

6
00:00:11,519 --> 00:00:15,599
lea instruction to be generated because

7
00:00:13,599 --> 00:00:17,680
basically we said the math like this

8
00:00:15,599 --> 00:00:19,840
visual studio would prefer to create an

9
00:00:17,680 --> 00:00:21,039
lea so that it could do the base plus

10
00:00:19,840 --> 00:00:23,119
index time scale

11
00:00:21,039 --> 00:00:24,880
plus displacement so let's see if we get

12
00:00:23,119 --> 00:00:30,160
the same thing again

13
00:00:24,880 --> 00:00:33,040
so let's compile it and debug it

14
00:00:30,160 --> 00:00:35,040
okay so standard function prolog

15
00:00:33,040 --> 00:00:35,840
although this time we see that it is

16
00:00:35,040 --> 00:00:38,719
choosing

17
00:00:35,840 --> 00:00:40,399
to subtract from rsp so it's not going

18
00:00:38,719 --> 00:00:41,920
to be using the red zone for its stack

19
00:00:40,399 --> 00:00:42,879
frame it's going to create a more

20
00:00:41,920 --> 00:00:44,719
explicit one

21
00:00:42,879 --> 00:00:46,559
so actually I just realized we need to

22
00:00:44,719 --> 00:00:50,480
exit out because this takes an

23
00:00:46,559 --> 00:00:50,879
argvv of 1 so I was right to be doing

24
00:00:50,480 --> 00:00:53,600
this

25
00:00:50,879 --> 00:00:55,039
so we need to pass a argv of 1 into

26
00:00:53,600 --> 00:00:56,559
this thing so some number that's going

27
00:00:55,039 --> 00:00:59,760
to be passed to atoi

28
00:00:56,559 --> 00:01:02,239
and it from a string to an integer so

29
00:00:59,760 --> 00:01:03,600
let's pick a completely random number

30
00:01:02,239 --> 00:01:05,840
and let's restart it

31
00:01:03,600 --> 00:01:08,880
all right so stepping in all right right

32
00:01:05,840 --> 00:01:11,920
before subtracting 0x20 from the stack

33
00:01:08,880 --> 00:01:13,200
let's step one more and now we have the

34
00:01:11,920 --> 00:01:17,520
stack pointer down at

35
00:01:13,200 --> 00:01:19,600
df40 and the base pointer at df60

36
00:01:17,520 --> 00:01:20,720
so the first thing that's going to be

37
00:01:19,600 --> 00:01:24,720
done is moving

38
00:01:20,720 --> 00:01:25,600
edi to rbp minus 14. now we didn't see

39
00:01:24,720 --> 00:01:28,640
edi get

40
00:01:25,600 --> 00:01:28,960
set with anything because edi is one of

41
00:01:28,640 --> 00:01:31,119
the

42
00:01:28,960 --> 00:01:32,000
command was one of the arguments to the

43
00:01:31,119 --> 00:01:35,200
function main

44
00:01:32,000 --> 00:01:36,000
right so this main took an argc and an

45
00:01:35,200 --> 00:01:38,880
argv

46
00:01:36,000 --> 00:01:40,079
pointer to a pointer right so edi is the

47
00:01:38,880 --> 00:01:42,640
first argument

48
00:01:40,079 --> 00:01:43,280
it's going to be the argc so if we step

49
00:01:42,640 --> 00:01:46,640
over this

50
00:01:43,280 --> 00:01:49,840
we will see that edi or rdi was

51
00:01:46,640 --> 00:01:52,159
2 and it was set to rbp minus 14. oh

52
00:01:49,840 --> 00:01:54,240
i forgot to put my display i'm going to

53
00:01:52,159 --> 00:01:57,600
put a display of

54
00:01:54,240 --> 00:01:59,520
we're gonna make these as let's do

55
00:01:57,600 --> 00:02:01,040
8 bytes because this next thing's

56
00:01:59,520 --> 00:02:01,840
gonna be a pointer because it's putting

57
00:02:01,040 --> 00:02:04,320
rdi

58
00:02:01,840 --> 00:02:05,040
so 8 bytes at a time let's go with

59
00:02:04,320 --> 00:02:08,879
4

60
00:02:05,040 --> 00:02:12,879
giant words in hex at rbp

61
00:02:08,879 --> 00:02:16,239
minus 0x20 so this right there

62
00:02:12,879 --> 00:02:19,120
is rbp minus 8 10 and then

63
00:02:16,239 --> 00:02:20,640
14. it's the most significant bytes of

64
00:02:19,120 --> 00:02:22,560
that particular 8 byte

65
00:02:20,640 --> 00:02:24,319
value so that's the 2 that just got

66
00:02:22,560 --> 00:02:27,599
written the argc

67
00:02:24,319 --> 00:02:28,560
and next the argv which is a char star

68
00:02:27,599 --> 00:02:30,800
star it's a

69
00:02:28,560 --> 00:02:32,319
pointer to a pointer array is going to

70
00:02:30,800 --> 00:02:36,400
be put at rbp

71
00:02:32,319 --> 00:02:38,879
minus 20. so step over that and

72
00:02:36,400 --> 00:02:40,400
that's the value put to rbp minus 20.

73
00:02:38,879 --> 00:02:43,120
now if we wanted to see

74
00:02:40,400 --> 00:02:43,519
what our argv of 0 and argv of 1 looked

75
00:02:43,120 --> 00:02:46,080
like

76
00:02:43,519 --> 00:02:47,200
because we know it's a two element array

77
00:02:46,080 --> 00:02:50,080
we could take this

78
00:02:47,200 --> 00:02:51,760
and we could display it as two pointers

79
00:02:50,080 --> 00:02:54,879
so let's display

80
00:02:51,760 --> 00:02:57,040
two giant word pointers

81
00:02:54,879 --> 00:02:58,720
at that particular address and this is

82
00:02:57,040 --> 00:03:00,400
basically going to be argv of 0 and

83
00:02:58,720 --> 00:03:02,800
this is going to be argv of 1

84
00:03:00,400 --> 00:03:03,599
we know that by convention argv of 0

85
00:03:02,800 --> 00:03:05,760
is typically

86
00:03:03,599 --> 00:03:07,040
in for normal programs it's filled in

87
00:03:05,760 --> 00:03:09,519
with the

88
00:03:07,040 --> 00:03:10,560
name of the func of the executable

89
00:03:09,519 --> 00:03:13,680
that's actually running

90
00:03:10,560 --> 00:03:15,120
so if we examined this as a string and

91
00:03:13,680 --> 00:03:17,040
then we put that address in we would

92
00:03:15,120 --> 00:03:18,720
expect to see the name of the executable

93
00:03:17,040 --> 00:03:20,400
and indeed that's what we see the full

94
00:03:18,720 --> 00:03:21,040
path to the name of the executable

95
00:03:20,400 --> 00:03:23,440
because we just

96
00:03:21,040 --> 00:03:25,519
let it be the default a.out and then

97
00:03:23,440 --> 00:03:28,720
we expect that this argv of 1

98
00:03:25,519 --> 00:03:31,120
should be a string version of the number

99
00:03:28,720 --> 00:03:32,799
that we passed as the first parameter

100
00:03:31,120 --> 00:03:35,040
31337 all right so

101
00:03:32,799 --> 00:03:36,799
that's what we would expect the argv

102
00:03:35,040 --> 00:03:38,959
of 0 and argv of 1

103
00:03:36,799 --> 00:03:40,000
they're getting stored off into this

104
00:03:38,959 --> 00:03:43,040
saved area

105
00:03:40,000 --> 00:03:46,400
on the stack so let's keep on trucking

106
00:03:43,040 --> 00:03:48,959
it's going to pull out the argv

107
00:03:46,400 --> 00:03:51,519
and put it into rax and then it's going

108
00:03:48,959 --> 00:03:53,360
to add 8 to that so basically the argv is

109
00:03:51,519 --> 00:03:55,280
going to be this and then add 8

110
00:03:53,360 --> 00:03:57,519
means it's basically indexing in from

111
00:03:55,280 --> 00:03:59,920
argv of 0 which is what you'd get

112
00:03:57,519 --> 00:04:00,959
if you just took that address directly

113
00:03:59,920 --> 00:04:04,159
to argv

114
00:04:00,959 --> 00:04:06,400
of 1 so step over that add

115
00:04:04,159 --> 00:04:07,920
8 and now it's going to actually

116
00:04:06,400 --> 00:04:09,280
dereference that address

117
00:04:07,920 --> 00:04:11,599
because basically it's holding the

118
00:04:09,280 --> 00:04:13,280
address pointing to argv of 1 but it

119
00:04:11,599 --> 00:04:14,080
needs to dereference that to get the

120
00:04:13,280 --> 00:04:16,079
pointer out

121
00:04:14,080 --> 00:04:17,359
the pointer that is pointing to the

122
00:04:16,079 --> 00:04:21,280
actual string

123
00:04:17,359 --> 00:04:24,320
so step over that that's in rax now

124
00:04:21,280 --> 00:04:26,160
and as you know again just to confirm to

125
00:04:24,320 --> 00:04:28,160
display that as a string that is the

126
00:04:26,160 --> 00:04:29,680
string for argv 1 like we would expect

127
00:04:28,160 --> 00:04:31,440
that's being passed as the first

128
00:04:29,680 --> 00:04:33,360
argument into atoi

129
00:04:31,440 --> 00:04:34,720
and so to be the first argument it needs

130
00:04:33,360 --> 00:04:37,280
to be moved from rax

131
00:04:34,720 --> 00:04:38,560
into rdi the register that's used for

132
00:04:37,280 --> 00:04:41,040
passing the first argument

133
00:04:38,560 --> 00:04:43,199
so step to that and then let's step over

134
00:04:41,040 --> 00:04:44,400
let's do next instruction to step over

135
00:04:43,199 --> 00:04:48,000
this call to ati

136
00:04:44,400 --> 00:04:52,360
atoi and then the rax value after

137
00:04:48,000 --> 00:04:53,680
atoi is the return value and so 7a69 is

138
00:04:52,360 --> 00:04:55,840
31337

139
00:04:53,680 --> 00:04:57,600
in hex so going back and looking at our

140
00:04:55,840 --> 00:05:01,199
source code for a second

141
00:04:57,600 --> 00:05:03,039
the output of atoi was put into a

142
00:05:01,199 --> 00:05:04,560
and then we're going to expect two times

143
00:05:03,039 --> 00:05:06,720
argc plus a

144
00:05:04,560 --> 00:05:07,600
coming up next okay so it's going to

145
00:05:06,720 --> 00:05:10,160
take the

146
00:05:07,600 --> 00:05:11,680
output of the atoi and put it into a so

147
00:05:10,160 --> 00:05:12,479
this would be where we would expect the

148
00:05:11,680 --> 00:05:15,280
a variable

149
00:05:12,479 --> 00:05:16,720
let's do that and we see it right there

150
00:05:15,280 --> 00:05:20,479
and then now

151
00:05:16,720 --> 00:05:22,479
rbp minus 14 is where the argc

152
00:05:20,479 --> 00:05:24,720
was stored before when it was storing

153
00:05:22,479 --> 00:05:26,880
the parameters passed into the function

154
00:05:24,720 --> 00:05:29,039
so it's pulling that out getting argc

155
00:05:26,880 --> 00:05:31,759
back out putting into rax

156
00:05:29,039 --> 00:05:33,440
and then now it does have an lea but

157
00:05:31,759 --> 00:05:35,600
it's not the same sort of lea

158
00:05:33,440 --> 00:05:38,479
as we saw before whereas before visual

159
00:05:35,600 --> 00:05:42,240
studio set the base to the a and the

160
00:05:38,479 --> 00:05:45,199
index equal to the argc and the scale

161
00:05:42,240 --> 00:05:46,960
to be 2 because 2 times argc this is

162
00:05:45,199 --> 00:05:50,160
essentially doing

163
00:05:46,960 --> 00:05:53,039
argc plus argc it's doing argc

164
00:05:50,160 --> 00:05:54,880
plus argc plus 1 or times 1 rather

165
00:05:53,039 --> 00:05:56,000
uh and so that's basically just the 2

166
00:05:54,880 --> 00:05:58,720
times argc

167
00:05:56,000 --> 00:05:59,039
using an lea as opposed to a multiply

168
00:05:58,720 --> 00:06:00,560
and

169
00:05:59,039 --> 00:06:02,880
so be it that's certainly one way you

170
00:06:00,560 --> 00:06:05,120
could do it and then it puts it into

171
00:06:02,880 --> 00:06:06,000
edx and we can see that it's going to

172
00:06:05,120 --> 00:06:09,360
then take

173
00:06:06,000 --> 00:06:12,000
the value that it is stored for

174
00:06:09,360 --> 00:06:13,600
a pull it out and do the addition so

175
00:06:12,000 --> 00:06:16,639
this was the 2 argc

176
00:06:13,600 --> 00:06:19,360
going into eax or sorry edx

177
00:06:16,639 --> 00:06:20,160
and then that plus eax is going to be

178
00:06:19,360 --> 00:06:22,880
the plus a

179
00:06:20,160 --> 00:06:23,360
so step over that step over that and

180
00:06:22,880 --> 00:06:26,319
we'll

181
00:06:23,360 --> 00:06:27,440
just say rdx is 4, 2 times 2 step

182
00:06:26,319 --> 00:06:29,520
over that

183
00:06:27,440 --> 00:06:31,199
that's our 31337

184
00:06:29,520 --> 00:06:34,560
and just add them together

185
00:06:31,199 --> 00:06:38,080
and the result goes into rax and so 7a

186
00:06:34,560 --> 00:06:38,800
6d and then now we have a new assembly

187
00:06:38,080 --> 00:06:41,919
instruction

188
00:06:38,800 --> 00:06:42,639
leave leave before the return so what is

189
00:06:41,919 --> 00:06:44,160
this doing

190
00:06:42,639 --> 00:06:45,680
now let's take a quick look at the stack

191
00:06:44,160 --> 00:06:48,639
again just to make sure

192
00:06:45,680 --> 00:06:50,800
so what leave does is it's basically

193
00:06:48,639 --> 00:06:52,240
very simply just taking what would be

194
00:06:50,800 --> 00:06:53,360
two assembly instructions and putting

195
00:06:52,240 --> 00:06:55,360
them into one

196
00:06:53,360 --> 00:06:56,639
that's the benefits of a complex

197
00:06:55,360 --> 00:06:59,840
architecture

198
00:06:56,639 --> 00:07:00,479
so basically you frequently would see in

199
00:06:59,840 --> 00:07:03,199
the past

200
00:07:00,479 --> 00:07:04,400
for unoptimized code that you know some

201
00:07:03,199 --> 00:07:07,759
compilers would generate

202
00:07:04,400 --> 00:07:08,400
instruction like move rbp to rsp and pop

203
00:07:07,759 --> 00:07:11,120
rbp

204
00:07:08,400 --> 00:07:13,120
because basically however much down the

205
00:07:11,120 --> 00:07:14,160
stack pointer has moved for creating the

206
00:07:13,120 --> 00:07:16,160
stack frame

207
00:07:14,160 --> 00:07:17,520
if the rbp is always pointing at the

208
00:07:16,160 --> 00:07:19,919
base of the stack frame

209
00:07:17,520 --> 00:07:20,800
then you just move the stack pointer up

210
00:07:19,919 --> 00:07:23,199
to the base

211
00:07:20,800 --> 00:07:24,880
and then you pop off the saved rbp so

212
00:07:23,199 --> 00:07:25,840
what did our stack frame look like in

213
00:07:24,880 --> 00:07:27,280
this example

214
00:07:25,840 --> 00:07:29,120
well basically there's the return

215
00:07:27,280 --> 00:07:30,800
address to get outside of main

216
00:07:29,120 --> 00:07:31,919
there's the saved rbp right at the

217
00:07:30,800 --> 00:07:33,039
beginning of maine and then the first

218
00:07:31,919 --> 00:07:36,560
thing it did was

219
00:07:33,039 --> 00:07:38,000
sub rsp 0x20 so it went down by 0x20

220
00:07:36,560 --> 00:07:40,400
allocated some space

221
00:07:38,000 --> 00:07:41,680
you got space for a which gets the a to

222
00:07:40,400 --> 00:07:43,680
atoi output

223
00:07:41,680 --> 00:07:45,759
and you've got the argc and argv the first

224
00:07:43,680 --> 00:07:48,160
and second parameter passed into main

225
00:07:45,759 --> 00:07:49,520
stored in this function parameter

226
00:07:48,160 --> 00:07:51,360
passing storage area

227
00:07:49,520 --> 00:07:53,120
red zone it's not touched at all in this

228
00:07:51,360 --> 00:07:54,080
code so basically you can see that the

229
00:07:53,120 --> 00:07:57,520
leave instruction

230
00:07:54,080 --> 00:08:00,560
by doing basically setting rsp equal to

231
00:07:57,520 --> 00:08:01,919
rbp it slides the rbp r sorry slides the

232
00:08:00,560 --> 00:08:04,479
rsp back up to here

233
00:08:01,919 --> 00:08:06,160
and then now the pop rbp is just going

234
00:08:04,479 --> 00:08:07,360
to take this old value and stick it back

235
00:08:06,160 --> 00:08:09,120
in before the return

236
00:08:07,360 --> 00:08:10,960
instruction just goes back to the

237
00:08:09,120 --> 00:08:12,000
previous code so basically you can just

238
00:08:10,960 --> 00:08:14,000
think of it like a

239
00:08:12,000 --> 00:08:16,000
leave is tearing down the stack frame

240
00:08:14,000 --> 00:08:16,319
all in one assembly instruction before

241
00:08:16,000 --> 00:08:18,560
you

242
00:08:16,319 --> 00:08:21,120
call the return instruction in order to

243
00:08:18,560 --> 00:08:22,479
return back to where you came from

244
00:08:21,120 --> 00:08:24,800
of course that doesn't exactly make

245
00:08:22,479 --> 00:08:25,280
thanos happy because he likes to see a

246
00:08:24,800 --> 00:08:26,720
good

247
00:08:25,280 --> 00:08:28,479
push at the beginning of a function and

248
00:08:26,720 --> 00:08:31,919
a pop at the end but

249
00:08:28,479 --> 00:08:34,080
you know so be it so we got another

250
00:08:31,919 --> 00:08:37,200
assembly instruction 31

251
00:08:34,080 --> 00:08:39,680
and boom let the confetti fly

252
00:08:37,200 --> 00:08:40,560
and this is something that we'll see in

253
00:08:39,680 --> 00:08:42,719
other

254
00:08:40,560 --> 00:08:43,680
functions because this just happens to

255
00:08:42,719 --> 00:08:48,080
be something that

256
00:08:43,680 --> 00:08:48,080
gcc prefers to generate

