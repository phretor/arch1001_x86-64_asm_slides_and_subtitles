1
00:00:00,160 --> 00:00:03,439
now let's see the same sort of control

2
00:00:02,240 --> 00:00:05,920
flow but using

3
00:00:03,439 --> 00:00:08,160
unsigned values instead in

4
00:00:05,920 --> 00:00:08,160
IfExample2

5
00:00:11,360 --> 00:00:15,040
right so unsigned int a and unsigned

6
00:00:14,240 --> 00:00:17,840
int b

7
00:00:15,040 --> 00:00:18,400
so that unsigned int a that negative

8
00:00:17,840 --> 00:00:20,720
1 is

9
00:00:18,400 --> 00:00:24,000
ffff that's going to be interpreted as

10
00:00:20,720 --> 00:00:26,960
just a unsigned value ffff so when we do

11
00:00:24,000 --> 00:00:29,359
is a equal to b that's not true but is a

12
00:00:26,960 --> 00:00:31,679
ffff greater than b

13
00:00:29,359 --> 00:00:34,079
yes that is true all f's is greater than

14
00:00:31,679 --> 00:00:35,920
two so we expect this one to return

15
00:00:34,079 --> 00:00:37,120
2 all the time and never return 3

16
00:00:35,920 --> 00:00:40,559
1 or 0xdefea7ed

17
00:00:37,120 --> 00:00:40,559
so let's compile that

18
00:00:41,840 --> 00:00:47,600
and debug it

19
00:00:45,440 --> 00:00:49,920
so there we go the same negative 1

20
00:00:47,600 --> 00:00:51,520
ffffs but it's going to be interpreted

21
00:00:49,920 --> 00:00:54,800
as a positive

22
00:00:51,520 --> 00:00:57,840
4 billion so let's step

23
00:00:54,800 --> 00:00:59,520
step step step step step step we don't

24
00:00:57,840 --> 00:01:02,800
expect the not equal to happen

25
00:00:59,520 --> 00:01:05,840
now here this is a below or equal

26
00:01:02,800 --> 00:01:07,840
check because it's a unsigned comparison

27
00:01:05,840 --> 00:01:08,960
so we use below and above instead of

28
00:01:07,840 --> 00:01:10,560
greater than less than when we're

29
00:01:08,960 --> 00:01:13,360
dealing with unsigned values

30
00:01:10,560 --> 00:01:13,840
so pull the negative 2 out which is

31
00:01:13,360 --> 00:01:17,520
actually

32
00:01:13,840 --> 00:01:20,479
all f's put it in eax

33
00:01:17,520 --> 00:01:20,960
there you go all f's and then compare it

34
00:01:20,479 --> 00:01:24,720
is

35
00:01:20,960 --> 00:01:26,320
all f's below 2 and the answer is

36
00:01:24,720 --> 00:01:28,159
no it is not so it's going to fall

37
00:01:26,320 --> 00:01:29,840
through it's not going to take the below

38
00:01:28,159 --> 00:01:33,360
or equal jump

39
00:01:29,840 --> 00:01:36,079
it will fall through and return 2

40
00:01:33,360 --> 00:01:36,640
step step unconditional jump to the end

41
00:01:36,079 --> 00:01:40,159
and then

42
00:01:36,640 --> 00:01:40,159
exit out of the function

