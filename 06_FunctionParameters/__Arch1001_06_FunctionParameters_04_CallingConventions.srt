0
00:00:00,240 --> 00:00:03,679
so when we dug into the microsoft

1
00:00:01,760 --> 00:00:05,520
documentation we saw talking about

2
00:00:03,679 --> 00:00:06,560
calling convention that it used with the

3
00:00:05,520 --> 00:00:08,320
shadow space

4
00:00:06,560 --> 00:00:09,920
so let's learn more about calling

5
00:00:08,320 --> 00:00:11,599
conventions in general

6
00:00:09,920 --> 00:00:13,519
first of all there's sort of two sub

7
00:00:11,599 --> 00:00:14,320
elements of importance in a calling

8
00:00:13,519 --> 00:00:16,560
convention

9
00:00:14,320 --> 00:00:18,960
the first are the register conventions

10
00:00:16,560 --> 00:00:21,520
about which registers belong to the

11
00:00:18,960 --> 00:00:23,760
caller versus callee function

12
00:00:21,520 --> 00:00:26,000
and the second is the parameter passing

13
00:00:23,760 --> 00:00:28,480
conventions of how a caller

14
00:00:26,000 --> 00:00:29,279
passes parameters to a callee

15
00:00:28,480 --> 00:00:32,320
and how the callee

16
00:00:29,279 --> 00:00:33,680
passes a return value back

17
00:00:32,320 --> 00:00:35,280
it's important to know that calling

18
00:00:33,680 --> 00:00:36,239
conventions are always compiler

19
00:00:35,280 --> 00:00:39,920
dependent

20
00:00:36,239 --> 00:00:42,000
so unless specifically some GCC compiler

21
00:00:39,920 --> 00:00:42,960
is trying to abide by a calling

22
00:00:42,000 --> 00:00:45,120
convention

23
00:00:42,960 --> 00:00:46,480
to work with microsoft code they will

24
00:00:45,120 --> 00:00:48,960
frequently generate

25
00:00:46,480 --> 00:00:50,399
mutually incompatible code this was much

26
00:00:48,960 --> 00:00:52,559
more of a problem when there

27
00:00:50,399 --> 00:00:53,440
were many many more compiler makers

28
00:00:52,559 --> 00:00:55,039
running around

29
00:00:53,440 --> 00:00:56,960
but over time the number of compiler

30
00:00:55,039 --> 00:00:58,320
makers has decreased

31
00:00:56,960 --> 00:01:01,120
in this class we're only going to be

32
00:00:58,320 --> 00:01:03,840
talking about the microsoft x64 ABI

33
00:01:01,120 --> 00:01:04,960
which is used by the compiler behind

34
00:01:03,840 --> 00:01:08,000
visual studio

35
00:01:04,960 --> 00:01:11,439
and the System V x86-x64 ABI

36
00:01:08,000 --> 00:01:14,240
which is used by

37
00:01:11,439 --> 00:01:15,680
Unix-derived systems like linux BSDs and

38
00:01:14,240 --> 00:01:17,759
also macOS

39
00:01:15,680 --> 00:01:19,520
the links to the ABI documentation are

40
00:01:17,759 --> 00:01:21,119
given at the bottom of this page

41
00:01:19,520 --> 00:01:23,439
and there's a lot of interesting bits

42
00:01:21,119 --> 00:01:25,520
and caveats and things that I'm just

43
00:01:23,439 --> 00:01:26,640
glossing over in this class so I

44
00:01:25,520 --> 00:01:29,280
recommend you check out that

45
00:01:26,640 --> 00:01:31,200
documentation later

46
00:01:29,280 --> 00:01:32,400
okay so the first sub element of a

47
00:01:31,200 --> 00:01:33,439
calling convention the register

48
00:01:32,400 --> 00:01:36,479
conventions

49
00:01:33,439 --> 00:01:38,560
we have the caller-saved registers

50
00:01:36,479 --> 00:01:39,680
these are also called volatile registers

51
00:01:38,560 --> 00:01:41,600
by microsoft

52
00:01:39,680 --> 00:01:43,119
because basically a caller, a function

53
00:01:41,600 --> 00:01:45,280
which is calling some other function,

54
00:01:43,119 --> 00:01:46,880
a caller should assume that they will be

55
00:01:45,280 --> 00:01:50,000
changed by the callee

56
00:01:46,880 --> 00:01:52,320
and therefore the caller is responsible

57
00:01:50,000 --> 00:01:53,520
for saving those registers before

58
00:01:52,320 --> 00:01:55,439
calling something

59
00:01:53,520 --> 00:01:56,640
so that it can restore them later on if

60
00:01:55,439 --> 00:01:57,759
it doesn't want to

61
00:01:56,640 --> 00:01:59,680
deal with the fact that they've been

62
00:01:57,759 --> 00:02:02,000
smashed by the callee

63
00:01:59,680 --> 00:02:03,360
so the conventions for visual studio is

64
00:02:02,000 --> 00:02:06,159
that these registers

65
00:02:03,360 --> 00:02:07,680
are the caller-save registers rax rcx

66
00:02:06,159 --> 00:02:10,879
rdx etc

67
00:02:07,680 --> 00:02:13,280
and for GCC these are the registers so

68
00:02:10,879 --> 00:02:14,560
these register lists only differ in

69
00:02:13,280 --> 00:02:18,400
terms of rdi

70
00:02:14,560 --> 00:02:19,040
and rsi so GCC and other System V

71
00:02:18,400 --> 00:02:22,400
derived

72
00:02:19,040 --> 00:02:26,160
ABI using compilers will save

73
00:02:22,400 --> 00:02:28,640
the rdi and rsi in the caller

74
00:02:26,160 --> 00:02:30,560
and so since visual studio doesn't

75
00:02:28,640 --> 00:02:31,840
that kind of implies that instead those two

76
00:02:30,560 --> 00:02:34,560
registers are callee-

77
00:02:31,840 --> 00:02:36,640
save registers so the callee-save

78
00:02:34,560 --> 00:02:37,599
register also called non-volatile

79
00:02:36,640 --> 00:02:39,120
registers

80
00:02:37,599 --> 00:02:40,959
are something where the caller should

81
00:02:39,120 --> 00:02:42,640
assume they will not be changed by the

82
00:02:40,959 --> 00:02:44,080
callee and if the callee is changing

83
00:02:42,640 --> 00:02:46,800
them they're misbehaving and

84
00:02:44,080 --> 00:02:48,560
could lead to broken code so these are

85
00:02:46,800 --> 00:02:51,519
registers that belong to the caller

86
00:02:48,560 --> 00:02:53,440
and therefore the callee needs to save

87
00:02:51,519 --> 00:02:54,400
them and restore them so that it doesn't

88
00:02:53,440 --> 00:02:56,480
break anything

89
00:02:54,400 --> 00:02:57,760
in the caller so again the register list

90
00:02:56,480 --> 00:03:00,959
is given below

91
00:02:57,760 --> 00:03:03,200
and it differs only in terms of rdi

92
00:03:00,959 --> 00:03:04,080
and rsi and when it comes to saving

93
00:03:03,200 --> 00:03:06,000
registers

94
00:03:04,080 --> 00:03:07,120
both the caller and the callee are

95
00:03:06,000 --> 00:03:09,440
responsible for

96
00:03:07,120 --> 00:03:11,200
balancing any register saves they

97
00:03:09,440 --> 00:03:12,000
perform where they're adding things to

98
00:03:11,200 --> 00:03:14,480
the stack

99
00:03:12,000 --> 00:03:15,440
with restores where they remove things

100
00:03:14,480 --> 00:03:17,200
from the stack

101
00:03:15,440 --> 00:03:19,200
this of course earns a thumbs up from Thanos

102
00:03:17,200 --> 00:03:21,280
the caller

103
00:03:19,200 --> 00:03:22,800
will typically like most assembly code

104
00:03:21,280 --> 00:03:23,760
you're going to see generated by these

105
00:03:22,800 --> 00:03:26,159
compilers

106
00:03:23,760 --> 00:03:28,319
it will typically save the registers

107
00:03:26,159 --> 00:03:30,560
save the caller-save registers

108
00:03:28,319 --> 00:03:32,480
right before a call and then it will

109
00:03:30,560 --> 00:03:33,120
typically restore them right after a

110
00:03:32,480 --> 00:03:35,920
call

111
00:03:33,120 --> 00:03:37,840
so sort of a way to infer what's going

112
00:03:35,920 --> 00:03:39,280
on if you see a bunch of register saves

113
00:03:37,840 --> 00:03:40,239
right before a call and you see a

114
00:03:39,280 --> 00:03:42,799
balanced

115
00:03:40,239 --> 00:03:43,920
register restore right after the call

116
00:03:42,799 --> 00:03:46,720
that is the caller

117
00:03:43,920 --> 00:03:48,400
saving some caller-save registers and of

118
00:03:46,720 --> 00:03:48,799
course you can double check against the

119
00:03:48,400 --> 00:03:50,480
list

120
00:03:48,799 --> 00:03:52,239
to make sure that they are caller-save

121
00:03:50,480 --> 00:03:53,439
registers on the list that I gave

122
00:03:52,239 --> 00:03:57,040
previously

123
00:03:53,439 --> 00:03:58,480
similarly typically a callee will save

124
00:03:57,040 --> 00:03:59,040
registers right at the beginning of the

125
00:03:58,480 --> 00:04:00,799
function

126
00:03:59,040 --> 00:04:02,319
and it's going to restore them right at

127
00:04:00,799 --> 00:04:04,239
the end of the function

128
00:04:02,319 --> 00:04:06,239
so that again will be your hint that

129
00:04:04,239 --> 00:04:07,519
it's callee-save registers that are

130
00:04:06,239 --> 00:04:09,040
being saved and restored

131
00:04:07,519 --> 00:04:12,080
if you see it at the beginning and

132
00:04:09,040 --> 00:04:14,879
balanced at the end of the function

133
00:04:12,080 --> 00:04:16,479
so here's a visual representation of the

134
00:04:14,879 --> 00:04:18,479
different conventions in use

135
00:04:16,479 --> 00:04:20,799
we said that all of the registers are

136
00:04:18,479 --> 00:04:23,440
the same except rdi and rsi

137
00:04:20,799 --> 00:04:25,040
so a visual studio has these registers

138
00:04:23,440 --> 00:04:28,240
as caller-save

139
00:04:25,040 --> 00:04:29,199
and these registers as callee-save well

140
00:04:28,240 --> 00:04:32,240
it's the same

141
00:04:29,199 --> 00:04:35,199
for the System V derived things rax

142
00:04:32,240 --> 00:04:36,320
is caller-saved rcx is caller-saved rdx is

143
00:04:35,199 --> 00:04:39,919
caller-saved

144
00:04:36,320 --> 00:04:41,600
but they differ in terms of rsi and rdi

145
00:04:39,919 --> 00:04:43,199
now let's talk about the second sub

146
00:04:41,600 --> 00:04:46,639
element of a calling convention

147
00:04:43,199 --> 00:04:49,600
the parameter passing conventions so

148
00:04:46,639 --> 00:04:51,520
both the System V ABI and the visual

149
00:04:49,600 --> 00:04:54,960
studio or microsoft

150
00:04:51,520 --> 00:04:56,080
x64 ABI these compilers use a subset of

151
00:04:54,960 --> 00:04:59,040
the caller-

152
00:04:56,080 --> 00:05:00,400
save registers to pass parameters into

153
00:04:59,040 --> 00:05:03,600
and out of functions

154
00:05:00,400 --> 00:05:05,280
the first one rax both compilers use rax

155
00:05:03,600 --> 00:05:07,600
as a caller-save register

156
00:05:05,280 --> 00:05:09,199
rax is special in that it's always going

157
00:05:07,600 --> 00:05:12,320
to be passing back

158
00:05:09,199 --> 00:05:14,479
passing out the return value so if it's

159
00:05:12,320 --> 00:05:16,240
a 64-bit value or smaller which is

160
00:05:14,479 --> 00:05:18,639
typically what you're going to see

161
00:05:16,240 --> 00:05:19,919
then rax is actually acting by

162
00:05:18,639 --> 00:05:22,400
convention

163
00:05:19,919 --> 00:05:23,840
as the return value and so that's very

164
00:05:22,400 --> 00:05:24,960
common that you'll see things being

165
00:05:23,840 --> 00:05:27,520
moved into

166
00:05:24,960 --> 00:05:28,080
eax rax and so forth right before the

167
00:05:27,520 --> 00:05:30,400
return

168
00:05:28,080 --> 00:05:31,680
out of a function that's holding a

169
00:05:30,400 --> 00:05:33,680
return value

170
00:05:31,680 --> 00:05:34,800
it's a return value in the C sense of

171
00:05:33,680 --> 00:05:35,520
the word and that when you have a

172
00:05:34,800 --> 00:05:38,479
function

173
00:05:35,520 --> 00:05:40,080
it returns one return value it can also

174
00:05:38,479 --> 00:05:42,080
be the case like if you see this

175
00:05:40,080 --> 00:05:45,600
sometime and you're wondering what's up

176
00:05:42,080 --> 00:05:46,800
it can be the case that 128 bit value

177
00:05:45,600 --> 00:05:49,759
can be returned

178
00:05:46,800 --> 00:05:50,479
by using the concatenation of rdx for

179
00:05:49,759 --> 00:05:54,400
the top

180
00:05:50,479 --> 00:05:56,240
64 bits and rax for the bottom 64 bits

181
00:05:54,400 --> 00:05:57,680
so that's the common elements of both of

182
00:05:56,240 --> 00:05:59,360
the compilers now let's see the

183
00:05:57,680 --> 00:06:02,319
differences

184
00:05:59,360 --> 00:06:03,600
so in the microsoft x64 ABI the first

185
00:06:02,319 --> 00:06:06,560
four parameters

186
00:06:03,600 --> 00:06:10,240
four parameters from left to right are

187
00:06:06,560 --> 00:06:12,160
put into rcx rdx r8 r9 respectively

188
00:06:10,240 --> 00:06:13,360
now we already actually saw this in too

189
00:06:12,160 --> 00:06:15,360
many parameters

190
00:06:13,360 --> 00:06:16,560
you would see the value of "a" going into

191
00:06:15,360 --> 00:06:20,080
rcx, "b"

192
00:06:16,560 --> 00:06:22,960
going to rdx, "c" going to r8, "d" going

193
00:06:20,080 --> 00:06:23,759
to r9 and then any parameters past

194
00:06:22,960 --> 00:06:26,960
that

195
00:06:23,759 --> 00:06:28,880
"e" or if we add an "f", "g", "h", etc

196
00:06:26,960 --> 00:06:30,960
those parameters are pushed onto the

197
00:06:28,880 --> 00:06:32,479
stack so the remaining parameters are

198
00:06:30,960 --> 00:06:34,319
pushed onto the stack

199
00:06:32,479 --> 00:06:37,039
so that the leftmost parameter is at the

200
00:06:34,319 --> 00:06:38,479
lowest address essentially it's you know

201
00:06:37,039 --> 00:06:40,560
it would basically be pushed in the

202
00:06:38,479 --> 00:06:41,680
reverse order push push push

203
00:06:40,560 --> 00:06:43,759
and then this thing would be at the

204
00:06:41,680 --> 00:06:45,280
lowest address or just you know moving

205
00:06:43,759 --> 00:06:46,080
it on to the stack using mov

206
00:06:45,280 --> 00:06:48,400
instructions

207
00:06:46,080 --> 00:06:49,440
which is more typical by contrast the

208
00:06:48,400 --> 00:06:52,960
System V

209
00:06:49,440 --> 00:06:55,840
x86-64 ABI has

210
00:06:52,960 --> 00:06:57,680
six parameters from the caller-saved

211
00:06:55,840 --> 00:06:58,720
registers which can be used for passing

212
00:06:57,680 --> 00:07:02,319
parameters

213
00:06:58,720 --> 00:07:05,599
and the ordering is rdi rsi rdx

214
00:07:02,319 --> 00:07:07,440
rcx r8 r9 so in this quote unquote too

215
00:07:05,599 --> 00:07:09,120
many parameters we actually don't have

216
00:07:07,440 --> 00:07:12,639
too many parameters for the

217
00:07:09,120 --> 00:07:14,960
x86-64 ABI

218
00:07:12,639 --> 00:07:15,759
used on system V systems it would

219
00:07:14,960 --> 00:07:19,680
basically be

220
00:07:15,759 --> 00:07:21,440
rdi rsi rdx rcx r8

221
00:07:19,680 --> 00:07:24,400
and there would be yet another parameter

222
00:07:21,440 --> 00:07:25,840
just freely available to pass more stuff

223
00:07:24,400 --> 00:07:28,160
if necessary but

224
00:07:25,840 --> 00:07:29,039
if you added two more arguments to this

225
00:07:28,160 --> 00:07:32,400
function

226
00:07:29,039 --> 00:07:34,319
then this next one would be passed in r9

227
00:07:32,400 --> 00:07:35,440
and the next one after that would be

228
00:07:34,319 --> 00:07:37,120
pushed onto the stack

229
00:07:35,440 --> 00:07:39,199
notionally pushed onto the stack

230
00:07:37,120 --> 00:07:41,280
practically speaking frequently

231
00:07:39,199 --> 00:07:43,599
moved on to the stack and so if we

232
00:07:41,280 --> 00:07:45,280
expand out that table from before again

233
00:07:43,599 --> 00:07:48,160
you know everything's the same except

234
00:07:45,280 --> 00:07:48,879
rsi and rdi we just expanded some of

235
00:07:48,160 --> 00:07:51,280
this rh

236
00:07:48,879 --> 00:07:52,000
through r11 to make it so that it's more

237
00:07:51,280 --> 00:07:54,879
visible

238
00:07:52,000 --> 00:07:57,919
that visual studio passes argument 1 in

239
00:07:54,879 --> 00:07:58,800
rcx argument 2 and rdx argument 3 and r8

240
00:07:57,919 --> 00:08:02,400
argument 4

241
00:07:58,800 --> 00:08:04,960
in r9 and then System V you know

242
00:08:02,400 --> 00:08:06,720
it just got to be difficult here passing

243
00:08:04,960 --> 00:08:08,000
it sort of backwards from this

244
00:08:06,720 --> 00:08:11,840
visualization

245
00:08:08,000 --> 00:08:13,280
parameter 1 in rdi, 2 in rsi, 3 in rdx

246
00:08:11,840 --> 00:08:16,160
4 in rcx

247
00:08:13,280 --> 00:08:19,520
and then jump over here to r8 gets

248
00:08:16,160 --> 00:08:21,599
parameter 5, r9 gets parameter 6

249
00:08:19,520 --> 00:08:23,360
so I have no idea how they came up with

250
00:08:21,599 --> 00:08:24,960
this particular ordering maybe it was

251
00:08:23,360 --> 00:08:25,840
just to be different from intel

252
00:08:24,960 --> 00:08:28,560
different from

253
00:08:25,840 --> 00:08:30,560
microsoft. I don't know doesn't make any

254
00:08:28,560 --> 00:08:32,719
more sense if we look at it in this

255
00:08:30,560 --> 00:08:34,399
sort of registers view of the world the

256
00:08:32,719 --> 00:08:37,599
visual studio is one two

257
00:08:34,399 --> 00:08:40,560
three four sure great makes sense and

258
00:08:37,599 --> 00:08:42,159
the System V is one two hop up here

259
00:08:40,560 --> 00:08:45,360
backwards three four

260
00:08:42,159 --> 00:08:47,360
up over here downwards five six

261
00:08:45,360 --> 00:08:49,360
so I don't know how they came up with

262
00:08:47,360 --> 00:08:50,320
that if anyone knows and would like to

263
00:08:49,360 --> 00:08:53,120
enlighten me

264
00:08:50,320 --> 00:08:53,120
let me know

